FROM rocker/shiny

ARG SHINY_SERVERDIR=/srv/shiny-server

USER root

WORKDIR ${SHINY_SERVERDIR}
RUN rm -r *
# Build's context should be set in the App directory
COPY ./ ./

# Install packages with apt-get
RUN if [ -f "apt.sh" ]; then \
  apt-get update -qq; \
  sh apt.sh; \
  fi

# Install packages with apt-get
RUN if [ -f "apt.txt" ]; then \
  apt-get update -qq; \
  apt-get -y --no-install-recommends install `grep -v "^#" apt.txt | tr '\n' ' '`; \
  fi

# Install an R environment
RUN if [ -f "install.R" ]; then \
  Rscript install.R; \
  fi

# Finalize
RUN cp shiny-server.conf /etc/shiny-server/shiny-server.conf
EXPOSE 3838
CMD ["/usr/bin/shiny-server"]
