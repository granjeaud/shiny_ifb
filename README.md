# commands within the HOST

Here are commands to run this shiny server proof of concept of a Ubuntu/Debian virtual machine at IFB.

```
# copy the information to build the docker shiny server
git clone https://gitcrcm.marseille.inserm.fr/granjeaud/shiny_ifb.git
cd shiny_ifb/

# build the docker image
docker build -t shiny_ifb .
cd ..

# launch shiny-server
docker run --rm -p 3838:3838 -v $PWD/srv/shinylog/:/var/log/shiny-server/ shiny_ifb
```


# other commands

```
# launch shiny-server in server mode with a share of apps and logs
docker run --rm -p 3838:3838 -v $PWD/srv/shinyapps/:/srv/shiny-server/ -v $PWD/srv/shinylog/:/var/log/shiny-server/ shiny_ifb

# launch shiny-server in interactive mode for debugging
docker run -it --rm -p 3838:3838 -v $PWD/srv/shinylog/:/var/log/shiny-server/ shiny_ifb bash

# view all trials
docker image list
docker container list

# view all possible changes
more Dockerfile apt.sh apt.txt install.R
```


# Commands for debugging WITHIN the CONTAINER

```
# ^D = Ctrl-D
# install vi
apt-get install vim
# modify the config
vi /etc/shiny-server/shiny-server.conf
# run in background
/usr/bin/shiny-server &
# view log (update the file name to the latest one)
tail -f /var/log/shiny-server/shiny-server-shiny-20231107-115703-43095.log
# ^D to end tail and go back to the server
fg
# ^D to kill the server
# ^D to quit the container, everything is lost
```
